#!/bin/bash

python3 process_sentidata.py
python3 sentitest.py
python3 nblearn.py sentiment_training.txt sentiment.nb
python3 nbclassify.py sentiment.nb sentiment_testing.txt  >  sentiment.out

python3 process_data.py
python3 test.py
python3 nblearn.py spam_training.txt spam.nb
python3 nbclassify.py spam.nb spam_testing.txt  >  spam.out