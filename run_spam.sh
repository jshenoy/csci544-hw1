#!/bin/bash

python3 process_data.py
python3 process_dev.py > f_res
python3 nblearn.py spam_training.txt spam.nb
python3 nbclassify.py spam.nb spam_testing.txt  >  spam.out
python3 PRF.py f_res spam.out