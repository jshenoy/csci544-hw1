1.  

Spam dataset:

* Precision - SPAM: 0.959   HAM: 0.987
* Recall -      SPAM: 0.967   HAM: 0.985
* F-Score -   SPAM: 0.963   HAM: 0.986

Sentiment dataset:

* Precision - NEG: 0.825   POS: 0.867
* Recall -      NEG: 0.875   POS: 0.815
* F-Score -   NEG: 0.850   POS: 0.840
 

2.

SVM

Spam dataset:

* Precision -   SPAM: 0.969   HAM: 0.952
* Recall -        SPAM: 0.862   HAM: 0.99 
* F Score-      SPAM: 0.912   HAM: 0.970

Sentiment dataset:

* Precision-  NEG: 0.883   POS: 0.858
* Recall-       NEG: 0.853   POS: 0.887 
* F Score-    NEG: 0.868   POS: 0.872 

3.
I found a drop of f-score of about 10-20%. For both Parts (I and II), the spam dataset f-score went down to ~0.8 and sentiment dataset f-score dropped to ~0.79

This implies that not many features were captured during the training since the training set was not big enough.



-----------------
To run part 1 and part 2, just run the scripts run_sentiment.sh and run_spam.sh for development data.  Run the scripts run_test_submit.sh (part1) and run_test_submit_<packagename>.sh (part 2: packagename=svm or mega) to get results for final test data