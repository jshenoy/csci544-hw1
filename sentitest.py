from glob import glob
import os
import re
import codecs

testing_file = "sentiment_testing.txt"
data_path = "SENTIMENT_test/*.txt"


#Delete this?

if os.path.exists(testing_file):
	os.remove(testing_file)

dataset = glob(data_path)
dataset.sort()
features = list()

fpp = codecs.open(testing_file, 'a', 'latin_1')

# number of files read without errors
num_files = len(dataset);
num_errors = 0;

for message in dataset:
	with codecs.open(message, 'r', 'latin_1') as fp:

## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
		try:
			for fline in fp:
				#thisline = fline.strip()				
				#fpp.write( thisline )
				#for wrd in re.findall(r"[\w']+", fline):
				#	fpp.write(wrd + " ")
				for wrd in fline.split():
					fpp.write(wrd + " ")				
		except:
			num_errors += 1
			pass

		fpp.write("\n")
fpp.close()

#print (num_errors)