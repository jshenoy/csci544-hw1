import sys
import pickle
from nblearn import spam_with_pickles
from math import log
from math import exp
import codecs

def find_max(cnt):
	maxv = float("-inf")
	maxk = str()
	for key in cnt:
		if cnt[key] > maxv:
			maxv = cnt[key]
			maxk = key

	return maxk


#todo
# 1. put loops
# 2. run process_data on dev
# 3. no need to exp later

#SVM
# longer hashing


paths = list()
for arg in sys.argv:
	paths.append(arg)

sp = pickle.load(open(paths[1],"rb"))
number_of_files = sp.nfiles
nwcat = sp.nwcat # number of words in each category
number_of_categories = sp.ncat
categorized_occ = sp.catocc
occwcat = sp.occwcat
superwc = sp.superwc

#print (str(len(superwc)))

tf = codecs.open(paths[2],'r', 'latin_1')

prior = dict()
posterior = dict()
p_numer = dict()
p_denom = 0.000
a = dict()
cd = dict()
classifed = str()
p = dict()

lsize = len(superwc)

for line in tf:



	words_line = line.split()

	#print (line)
	#print (words_line)
	#print (lsize)

	for key in categorized_occ:

		prior[key] = categorized_occ[key]/number_of_files

		prior[key] = log(prior[key]) # take log of prior

		posterior[key] = 0.000

		for wrd in words_line:
			posterior[key] += log( (occwcat[key][wrd]+1) ) - log(nwcat[key]+lsize) #with add-1 smoothing

		p_numer[key] = prior[key] + posterior[key]


	#for key in categorized_occ:
	#	p_denom +=  exp( prior[key] + posterior[key] )
	
	#print ("Denom " + str(posterior) + "\n")
	#p_denom = log(p_denom)

	for key in categorized_occ:
		p[key] = p_numer[key]

	#print ("Filename=" + line + "\n")
	print (find_max(p))

	#print ("Probs: " + str(p) + "\n")
	#print ("Priors: " + str(prior) + "\n")
	#print ("Posts: " + str(posterior) + "\n")
	#print ("Pnumer: " + str(p_numer) + "\n")
	#print ("Pdenom: " + str(p_denom) + "\n")
	#print (classifed + "\n")
tf.close()