#!/bin/bash

python3 process_sentidata.py
python3 process_sentidev.py > f_senti_res
python3 nblearn.py sentiment_training.txt sentiment.nb
python3 nbclassify.py sentiment.nb sentiment_testing.txt  >  sentiment.out
python3 PRF_senti.py f_senti_res sentiment.out