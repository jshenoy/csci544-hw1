import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
#learns and generates spam.nb

#todo:
# 1. Improve sentiment nb
# 1. support unicode
# 1. move add-smoothing to nblearn.py
# 2. generalize process_data.py
# 3. log -X
# 4. partition 65:35 and see result - little bit better
# 5. ccustom hashing - generae uniq word for each num -X
# 6. explore options for svm and megam. svmkernel -t 2/3


class spam_with_pickles:
	def __init__(self, nfiles, nwcat, ncat, catocc, occwcat, superwc):
		# nfiles: number of files (used)
		# nwcat: number of words in each category (used)
		# ncat: number of categories
		# catocc: number of files each category (used)
		# occwcat: occurence of word in each category (used)
		self.nfiles = nfiles
		self.nwcat = nwcat
		self.ncat = ncat
		self.catocc = catocc
		self.occwcat = occwcat
		self.superwc = superwc


if __name__ == "__main__":
	def word_counter(filenames):
		wc = 0
		number_of_files = 0
		category_occ = Counter() # number of files in each category
		nwcat = Counter() # number of words in each category
		occwcat = defaultdict(Counter) # number of times a word appears in each category
		superwc = Counter()
		# e.g. pharma occurs 5 times in SPAM and twice in HAM

		with codecs.open(filenames[1],'r', 'latin_1') as fin:

			for line in fin:
				number_of_files += 1 # same as line_num

				words = line.split()

				# size of each category
				category_occ[words[0]] += 1

				#wc = len(words)
				for wrd in words[1:]:
					occwcat[words[0]][wrd] += 1
					nwcat[words[0]] += 1
					superwc[wrd] += 1

		# number_of_files - nfiles
		# nwcat
		# len(category_occ) - ncat
		# category_occ - catocc
		# occwcat
		
		sp = spam_with_pickles(number_of_files, nwcat, len(category_occ), category_occ, occwcat, superwc)		
		#pickle.dump(sp, open("delthis.p","wb"))
		pickle.dump(sp, open(filenames[2],"wb"))



	paths = list()
	for arg in sys.argv:
		paths.append(arg)

	word_counter(paths)
