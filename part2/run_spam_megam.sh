#!/bin/bash

python3 format_spamdata.py
python3 format_spamtest.py
python3 spamtest_megam.py
./megam_i686 -nc multiclass spam_training.txt > spam.megam.model
./megam_i686 -nc -predict spam.megam.model multiclass spam_testing.txt > spam.megam.out2
python3 sanitize_spam_megam.py spam.megam.out2