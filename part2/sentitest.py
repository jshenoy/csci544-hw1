from glob import glob
import os
import re
import codecs
from collections import Counter, OrderedDict
import hashlib
import pickle

training_file = "sentiment_testing.txt"
data_path = "SENTIMENT_test/*.txt"

mywrd = pickle.load(open("words","rb"))

#Delete this?

if os.path.exists(training_file):
	os.remove(training_file)

dataset = glob(data_path)
features = list()

dataset.sort()

fpp = codecs.open(training_file, 'a','latin_1')

# number of files read without errors
num_files = len(dataset);
num_errors = 0;

for message in dataset:
	with codecs.open(message, 'r', 'latin_1') as fp:


		word_count = Counter()

		fpp.write("0 ")


		thisline = str()
		tempdict = dict()
		tf = Counter()

## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
		for fline in fp:
			#thisline = fline.strip()
			#fpp.write( thisline.encode("UTF-8") )

			for wrd in re.findall(r"[\w']+", fline):
				if wrd not in mywrd:
					mywrd[wrd] = len(mywrd) + 1
					tf[wrd] += 1
				else:
					tf[wrd] += 1


		for terms in tf:
				#thisline += " " + str(iwrd[terms]) +":"+ str(tf[terms])
			tempdict[mywrd[terms]] = tf[terms]

		sortdict = OrderedDict(sorted(tempdict.items()))

		for terms in sortdict:
			thisline += " " + str(terms) +":"+ str(sortdict[terms])

		fpp.write(thisline)


			#fpp.write(str(mywrd))
			#fpp.write(str(tf))

		fpp.write("\n")

		#except:
			#print (message)
		#	num_errors += 1
		#	pass
fpp.close()
#num_files_noerr = num_files - num_errors
#print ( "Size of dataset:" + str(num_errors