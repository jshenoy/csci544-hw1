from glob import glob
import os
import re
import codecs
from collections import Counter, OrderedDict
import hashlib

training_file = "sentiment_training.txt"
data_path = "SENTIMENT_test/*.txt"


#Delete this?

dataset = glob(data_path)
features = list()

dataset.sort()

fpp = codecs.open(training_file, 'a', 'latin_1' )

# number of files read without errors
num_files = len(dataset);
num_errors = 0;

fpp.write("TEST\n")


for message in dataset:
	with codecs.open(message, 'r', 'latin_1') as fp:


		word_count = Counter()

		fpp.write("POS ")


## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
		try:
			for fline in fp:
				#for wrd in re.findall(r"[\w']+", fline):
				#	fpp.write(wrd + " ")
				for wrd in fline.split():
					wrd2 = wrd.replace("#","")
					fpp.write(wrd2+" ")

		except:
			#print (message)
			num_errors += 1
			pass


	fpp.write("\n")

fpp.close()
#num_files_noerr = num_files - num_errors
#print ( "Size of dataset:" + str(num_errors))

