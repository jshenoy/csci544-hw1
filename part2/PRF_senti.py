from collections import Counter
import sys


paths = list()
for arg in sys.argv:
	paths.append(arg)

actual = open(paths[1],"r") #f_res - expected
predicted = open(paths[2],"r") #res - predicted

numer = Counter()
denom_precision = Counter()
denom_recall = Counter()
precision = Counter()
recall = Counter()
f_score = Counter()

numer["POS"] = 0
numer["NEG"] = 0

denom_precision["POS"] = 0
denom_precision["NEG"] = 0

denom_recall["POS"] = 0
denom_recall["NEG"] = 0

for a,p in zip(actual,predicted):
	a = a.strip()
	p = p.strip()
	for key in numer:
		if a==p and key==a:
			numer[key] += 1

		#print (p.strip() + " " +  key.strip() + " " + str(p==key))

		if p==key:
			denom_precision[key] += 1

		if a==key:
			denom_recall[key] += 1

for key in numer:
	precision[key] = numer[key]/denom_precision[key]
	recall[key] = numer[key]/denom_recall[key]
	f_score[key] = ( 2 * precision[key] * recall[key] )/ (precision[key] + recall[key])

print ("Precision: " + str(precision))
print ("Recall: " + str(recall))
print ("F Score: " + str(f_score))

actual.close()
predicted.close()