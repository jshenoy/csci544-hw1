#!/bin/bash

python3 process_spamdata.py
python3 process_spamtest.py > f_res_spam
./svm_learn spam_training.txt spam.svm.model
./svm_classify spam_testing.txt spam.svm.model spam.svm.out2
python3 sanitize_svm.py spam.svm.out2
python3 PRF.py f_res_spam spam.svm.out