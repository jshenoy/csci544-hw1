from glob import glob
import os
import re
import codecs
from collections import Counter, OrderedDict
import hashlib

training_file = "sentiment_training.txt"
data_path = "SENTIMENT_training/*.txt"


#Delete this?

if os.path.exists(training_file):
	os.remove(training_file)

dataset = glob(data_path)
features = list()

dataset.sort()

fpp = codecs.open(training_file, 'w', 'latin_1' )

# number of files read without errors
num_files = len(dataset);
num_errors = 0;





for message in dataset:
	with codecs.open(message,'r', 'latin_1') as fp:


		word_count = Counter()

		if re.match('.*NEG\..*\.txt',message) is not None:
			fpp.write("NEG ")
		elif re.match('.*POS\..*\.txt',message) is not None:
			fpp.write("POS ")
		else:
			assert("Check filename")

## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
		try:
			for fline in fp:
				#for wrd in re.findall(r"[\w']+", fline):
				#	fpp.write(wrd + " ")
				for wrd in fline.split():
					wrd2 = wrd.replace("#","")
					fpp.write(wrd2 +" ")

		except:
			#print (message)
			num_errors += 1
			pass


	fpp.write("\n")

fpp.close()
#num_files_noerr = num_files - num_errors
#print ( "Size of dataset:" + str(num_errors))

