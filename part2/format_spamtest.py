from glob import glob
import os
import re
import codecs
from collections import Counter, OrderedDict
import hashlib

training_file = "spam_training.txt"
data_path = "SPAM_dev/*.txt"


#Delete this?


dataset = glob(data_path)
features = list()

dataset.sort()

fpp = codecs.open(training_file, 'a','latin_1')

# number of files read without errors
num_files = len(dataset);
num_errors = 0;
fpp.write("DEV\n")
for message in dataset:
	with codecs.open(message, 'r', 'latin_1') as fp:


		word_count = Counter()

		if re.match('.*SPAM\..*\.txt',message) is not None:
			fpp.write("SPAM ")
		elif re.match('.*HAM\..*\.txt',message) is not None:
			fpp.write("HAM ")
		else:
			assert("Check filename")

## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
		try:
			for fline in fp:
				for wrd in re.findall(r"[\w']+", fline):
					fpp.write(wrd + " ")


		except:
			#print (message)
			num_errors += 1
			pass

	fpp.write("\n")

fpp.close()
#num_files_noerr = num_files - num_errors
#print ( "Size of dataset:" + str(num_errors))

