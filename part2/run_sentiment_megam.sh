#!/bin/bash
python3 format_sentidata.py
python3 format_sentitest.py
python3 sentitest_megam.py
./megam_i686 -nc multiclass sentiment_training.txt > sentiment.megam.model
./megam_i686 -predict sentiment.megam.model multiclass sentiment_testing.txt > sentiment.megam.out2
python3 sanitize_megam.py sentiment.megam.out2
