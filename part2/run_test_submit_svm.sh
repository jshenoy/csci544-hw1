#!/bin/bash

python3 process_spamdata.py
python3 spamtest.py
./svm_learn spam_training.txt spam.svm.model
./svm_classify spam_testing.txt spam.svm.model spam.svm.out2
python3 sanitize_svm.py spam.svm.out2

python3 process_sentidata.py
python3 sentitest.py
./svm_learn sentiment_training.txt sentiment.svm.model
./svm_classify sentiment_testing.txt sentiment.svm.model sentiment.svm.out2
python3 sanitize_svm.py sentiment.svm.out2