#!/bin/bash

python3 process_sentidata.py
python3 process_sentitest.py > f_res_sentiment
./svm_learn sentiment_training.txt sentiment.svm.model
./svm_classify sentiment_testing.txt sentiment.svm.model sentiment.svm.out2
python3 sanitize_svm.py sentiment.svm.out2
python3 PRF_senti.py f_res_sentiment sentiment.svm.out
