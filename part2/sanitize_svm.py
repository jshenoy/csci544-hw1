import sys, os

#Cleans output to resemble naive bayes
paths = list()
for arg in sys.argv:
	paths.append(arg)

intype = paths[1].split(".")[0]
algo = paths[1].split(".")[1]


fp = open(paths[1],"r")
outfile = intype + "." + algo + ".out"
fpout = open(outfile,"w")

if intype == "spam":
	ones = "HAM"
	zeros = "SPAM"
elif intype == "sentiment":
	ones = "POS"
	zeros = "NEG"
else:
	assert("What are you classifying???")

for line in fp:
	first_col = line.split()[0]
	if float(first_col.strip()) > 0:
		fpout.write(ones + "\n")
	elif float(first_col.strip()) < 0:
		fpout.write(zeros + "\n")
	else:
		assert("Unidentified class!!!")
		print ("yo")

fp.close()
fpout.close()
#os.remove(paths[1])
