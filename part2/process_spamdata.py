from glob import glob
import os
import re
import codecs
from collections import Counter, OrderedDict
import hashlib
import pickle


if __name__ == "__main__":
	iwrd = dict()

	training_file = "spam_training.txt"
	data_path = "SPAM_training/*.txt"


	#Delete this?

	if os.path.exists(training_file):
		os.remove(training_file)

	dataset = glob(data_path)
	features = list()

	dataset.sort()

	fpp = codecs.open(training_file, 'a','latin_1')

	# number of files read without errors
	num_files = len(dataset);
	num_errors = 0;


	for message in dataset:
		with codecs.open(message, 'r','latin_1') as fp:

			if re.match('.*SPAM\..*\.txt',message) is not None:
				fpp.write("-1 ")
			elif re.match('.*HAM\..*\.txt',message) is not None:
				fpp.write("1 ")
			else:
				assert("Check filename")

	## There must be a better way to do this! Read the unicode? Yes. Should figure out how to do this
			try:
				tf = Counter()
				thisline = str()
				tempdict = dict()

				for fline in fp:
				#thisline = fline.strip()
				#fpp.write( thisline.encode("UTF-8") )

					for wrd in re.findall(r"[\w']+", fline):
						if wrd not in iwrd:
							iwrd[wrd] = len(iwrd) + 1
							tf[wrd] += 1
						else:
							tf[wrd] += 1


				for terms in tf:
					#thisline += " " + str(iwrd[terms]) +":"+ str(tf[terms])
					tempdict[iwrd[terms]] = tf[terms]

				sortdict = OrderedDict(sorted(tempdict.items()))

				for terms in sortdict:
					thisline += " " + str(terms) +":"+ str(sortdict[terms])

				fpp.write(thisline)

				#fpp.write(str(iwrd))
				#fpp.write(str(tf))


			except:
				#print (message)
			#	num_errors += 1
				pass

		


		fpp.write("\n")

	fpp.close()

	pickle.dump(iwrd, open("words_spam","wb"))

#num_files_noerr = num_files - num_errors
#print ( "Size of dataset:" + str(num_errors))

